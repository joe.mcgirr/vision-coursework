function value = zero_normalize(image)
    value =  single(image(:) - mean(image(:)));
    %divide by stddev
    value = value(:) ./ norm(value);
end
