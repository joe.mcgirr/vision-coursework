function [disparity_map] = disparityEstimation(imageLeft, imageRight)
    disp_range=255;
    imshow(imageLeft);
    imageLeft=mean(imageLeft,3);
    imageRight=mean(imageRight,3);
    window_size = 4;
    %minus window_size
    disparity_map = zeros(size(imageLeft)-window_size, 'single');
    
    for yl = 1 : size(imageLeft, 1)-window_size
        for xl = 1 : size(imageLeft, 2) - window_size
            leftPixel = imageLeft(yl:yl+window_size-1, xl:xl+window_size-1);
                     
            %start searching 59 pixels behind leftPixel or from start of img
            search_start=max(-disp_range, 1-xl);
            %end search 59 pixels ahead of leftPixel or untill end of img
            search_end=min(disp_range, size(imageLeft,2)-(xl+window_size-1));
            window_count = search_end - search_start;
            similarity_array = zeros(1, window_count);
            
            for offset = search_start:search_end
                rightPixel = imageRight(yl:yl+window_size-1, xl+offset:xl+offset+window_size-1);
                pixelDifference = rightPixel - leftPixel;
                %gives index in relation to window_count
                index = offset-search_start + 1;
                similarity_array(1,index) = sum(abs(pixelDifference(:)));
                %recordSimilarity
            end
            [~,matched_index] = min(similarity_array(:));
            %divide by 4 to as images are quarter size
            disparity_map(yl,xl) = abs(matched_index+search_start-1);
        end
    end  
    figure('Name', '.Disparity Map')
    %disparity_map = uint8(disparity_map);
    imshow(disparity_map);
    colormap jet;
    colorbar ;
    caxis([0 255]);
    title('Depth map from  block matching');
    colormapeditor

    
